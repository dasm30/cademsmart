import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  FlatList,
  View,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { LineChart, YAxis, XAxis } from 'react-native-svg-charts';
import PropTypes from 'prop-types';
import moment from 'moment';
import { fetchWeathers } from '../../../redux/actions/weatherActions';

import { CADEM_COLOR_BLUE } from '../../constants';

class Weather extends Component {
  componentDidMount() {
    this.props.fetchWeathers()
  }
  renderWeather(cityData) {
    const city = cityData.city.name;
    const temps = _.map(cityData.list, weather => weather.main.temp).slice(0, 6);
    const time = cityData.list.map(weather => moment(weather.dt_txt).toDate()).slice(0, 6);
    const icon = cityData.list.map(weather => weather.weather.map(pronos => pronos.icon))
      .slice(0, 1);
    const weatherIcon = `http://openweathermap.org/img/w/${icon}.png`;
    const humedad = cityData.list.map(weather => weather.main.humidity).slice(0, 6);

    const avgTemp = _.round(_.sum(temps.slice(0, 3)) / temps.slice(0, 3).length);
    const avgHum = _.round(_.sum(humedad) / humedad.length);

    const contentInset = { top: 20, bottom: 20 };

    return (
      <View key={city} style={styles.chartContainer}>
        <Text style={styles.text}>Ciudad: {city} </Text>
        <Text style={styles.text}>Temperatura: {avgTemp} </Text>
        <Text style={styles.text}>Humedad: {avgHum} </Text>
        <Image style={{ width: 50, height: 50 }} source={{ uri: weatherIcon }} />

        <View style={{ width: 300, height: 200, flexDirection: 'row' }}>
          <YAxis
            data={temps}
            contentInset={contentInset}
            svg={{
              fill: 'grey',
              fontSize: 10,
            }}
            formatLabel={value => `${value}ºC`}
          />
          <LineChart
            style={{ flex: 1, marginLeft: 5 }}
            data={temps}
            svg={{ stroke: CADEM_COLOR_BLUE }}
            contentInset={contentInset}
          />
        </View>
        <View style={{ height: 10 }}>
          <XAxis
            style={{ marginHorizontal: -10, marginLeft: 30 }}
            data={time}
            xAccessor={({ item }) => item}
            formatLabel={(value) => moment(value).format('HH:mm')}
            contentInset={{ left: 20, right: 20 }}
            svg={{
              fill: 'grey',
              fontSize: 10,
            }}
          />
        </View>
      </View>
    );
  }
  renderItem = ({ item }) => this.renderWeather(item)
  render() {
    const { weathers, fetching } = this.props
    if (fetching) {
      return (
        <View style={styles.centeredContainer}>
          <ActivityIndicator size="large" color="#33A1FD" />
        </View>
      )
    }
    error = _.find(weathers, w => w.cod !== '200')
    if (!weathers || (weathers && error)) {
      return (
        <View style={styles.centeredContainer}>
          <Text style={styles.title}>
            {(error.message) || 'No Data'}
          </Text>
        </View>
      )
    }
    return (
      <FlatList
        style={styles.scrollContainer}
        contentContainerStyle={styles.scrollContentContainer}
        data={weathers}
        renderItem={this.renderItem}
        keyExtractor={(item) => `${item.city.id}`}
      />
    );
  }
}

const mapStateToProps = ({ weather }) => ({
  weathers: weather.weathers,
  response: weather.response,
  fetching: weather.fetching
});
const mapDispatchToProps = dispatch => ({
  fetchWeathers: (ids) => dispatch(fetchWeathers(ids))
})

export default connect(mapStateToProps, mapDispatchToProps)(Weather);

Weather.propTypes = {
  weathers: PropTypes.array,
};

const styles = StyleSheet.create({
  centeredContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  scrollContainer: {
    marginTop: 40,
  },
  scrollContentContainer: {
    padding: 20,
    paddingTop: 0,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  text: {
    color: '#333333',
    marginBottom: 5,
  },
  chartContainer: {
    marginTop: 20,
    marginBottom: 10,
  },
});