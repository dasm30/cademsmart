import { apiURL, apiKey } from './config'
import { validateResponse } from './utils/validations'
import { map } from 'lodash'
import cachedFetch from './utils/cachedFetch'

const endpoint = 'data/2.5/forecast'
const defaultCitiesIds = ['524901', '2613357', '6460975']

export async function getWeather(id = '524901') {
  const url = `${apiURL}/${endpoint}?id=${id}&APPID=${apiKey}&units=metric`
  const response = await cachedFetch(url, 600)
  return validateResponse(response)
}

export async function getWeathers(ids = defaultCitiesIds) {
  const promises = await map(ids, async id => {
    return await getWeather(id)
  })
  return await Promise.all(promises)
}