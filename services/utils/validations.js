export async function validateResponse(res) {
  res = await res.json();
  if (res.error) {
    console.error('Response error:', res);
  }
  else console.log('Response:',res);
  return res;
}