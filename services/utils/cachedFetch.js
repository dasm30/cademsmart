import { AsyncStorage } from 'react-native'

_storeData = async (key, value, callback) => {
  try {
    await AsyncStorage.setItem(key, value, callback);
  } catch (error) {
    // Error saving data
    console.error(error)
  }
}

_retrieveData = async (key, callback) => {
  try {
    const value = await AsyncStorage.getItem(key, callback);
    if (value !== null) {
      // We have data!!
      console.log(value);
    }
  } catch (error) {
    // Error retrieving data
    console.error(error)
  }
}

_removeData = async (key, callback) => {
  try {
    await AsyncStorage.removeItem(key, callback);
  } catch (error) {
    // Error removing data
    console.error(error)
  }
}

const cachedFetch = async (url, options) => {
  let expiry = 5 * 60 // 5 min default
  if (typeof options === 'number') {
    expiry = options
    options = undefined
  } else if (typeof options === 'object') {
    // I hope you didn't set it to 0 seconds
    expiry = options.seconds || expiry
  }
  // Use the URL as the cache key to sessionStorage
  let cacheKey = url
  let cached = await _retrieveData(cacheKey)
  let whenCached = await _retrieveData(cacheKey + ':ts')
  if (cached !== null && whenCached !== null) {
    // it was in sessionStorage! Yay!
    // Even though 'whenCached' is a string, this operation
    // works because the minus sign tries to convert the
    // string to an integer and it will work.
    let age = (Date.now() - whenCached) / 1000
    if (age < expiry) {
      let ct = await _retrieveData(cacheKey + ':ct')
      let blob
      if (ct && !(ct.match(/application\/json/i) || ct.match(/text\//i))) {
        console.log(cached)
        blob = dataURItoBlob(cached)
      } else {
        blob = new Blob([cached])
      }
      let response = new Response(blob)
      return await Promise.resolve(response)
    } else {
      // We need to clean up this old key
      _removeData(cacheKey)
      _removeData(cacheKey + ':ts')
      _removeData(cacheKey + ':ct')
    }
  }

  const response = await fetch(url, options)
  // let's only store in cache if the content-type is 
  // JSON or something non-binary
  if (response.status === 200) {
    let ct = response.headers.get('Content-Type')
    if (ct && (ct.match(/application\/json/i) || ct.match(/text\//i))) {
      // There is a .json() instead of .text() but 
      // we're going to store it in sessionStorage as 
      // string anyway.
      // If we don't clone the response, it will be 
      // consumed by the time it's returned. This 
      // way we're being un-intrusive. 
      const content = await response.clone().text()
      _storeData(cacheKey, content)
      _storeData(cacheKey + ':ts', Date.now().toString())
    } else if (ct) {
      const blob = response.clone().blob()
      let reader = new window.FileReader()
      reader.readAsDataURL(blob)
      reader.onloadend = () => {
        _storeData(cacheKey, reader.result)
        _storeData(cacheKey + ':ts', Date.now().toString())
        _storeData(cacheKey + ':ct', blob.type)
      }
    }
  }
  return response
}

// Thank you https://gist.github.com/fupslot/5015897#gistcomment-1580216
function dataURItoBlob(dataURI, callback) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var bb = new Blob([ab]);
  return bb;
}

export default cachedFetch;