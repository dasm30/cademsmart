## Comentarios de la prueba

Probado solo en iOS.

# Estructura del proyecto

Se organizó las carpetas de **redux** y **services** fuera de src pensando en una aplicación multiplataforma ubicada en el mismo directorio padre. Con esta estructura es posible reutilizar ,de forma organizada, redux y llamadas a los API entre una app con versiones tanto móvil como web. 

Lo ideal sería tener el proyecto en un directorio padre que contenta una carpeta mobile y otra web, junto con las carpetas redux y services.


## ------------------------------------------------------------------


El siguiente código, representa un Componente de React Native

## Para realizar el PUSH, Favor utilziar GitFlow y SUBIR EL FEATURE, NO hacer MERGE en Develop / Master


Este código, recibe desde el Store de Redux la respuesta del API Rest de OpenWeather.

Requerimientos:

- Incluir Action y Reducer necesario para obtener el objeto.
- Se debe corregir el error al renderizar el XAxis con la hora de medicion.
- Refactorizar el codigo para su correcta implementación.

Como información adicional, se utiliza el ESLINT de Airbnb en la versión ^12.1.0 (Modificado).
