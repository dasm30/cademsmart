import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import weather from './reducers/weather'

const store = createStore(
  combineReducers({
    weather
  }),
  applyMiddleware(thunk)
)

export default store
