import {
  FETCH_WEATHER,
  FETCH_WEATHERS,
} from './actionTypes'
import { getWeather, getWeathers } from '../../services/weather'

const handleResponse = (dispatch, state, res) => {
  state.payload = {
    fetching: false,
    weathers: res,
  }
  dispatch(state)
}

export const fetchWeather = (id = '524901') => async dispatch => {
  const newState = {
    type: FETCH_WEATHER,
    payload: {
      fetching: true,
    }
  }
  dispatch(newState)
  const res = await getWeather(id)
  handleResponse(dispatch, newState, res)
}

export const fetchWeathers = (ids) => async dispatch => {
  const newState = {
    type: FETCH_WEATHERS,
    payload: {
      fetching: true,
    }
  }
  dispatch(newState)
  const res = await getWeathers(ids)
  handleResponse(dispatch, newState, res)
}