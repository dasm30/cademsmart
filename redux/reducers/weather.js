import {
  FETCH_WEATHER,
  FETCH_WEATHERS,
} from '../actions/actionTypes'
const initialState = {
  /**Last fetched weather */
  weather: {},
  weathers: [],
  fetching: false,
}

export const weather = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_WEATHER: return { ...state, ...action.payload }
    case FETCH_WEATHERS: return { ...state, ...action.payload }
  }
  return state
}

export default weather